import { Injectable } from '@angular/core';
import { LoadingController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class LoadingService {

  loading;
  constructor(private loadingCtrl: LoadingController) { }

  async prLoading(title: string) {
    this.loading = await this.loadingCtrl.create({
      spinner: 'bubbles',
      message: title,
      translucent: true,
      cssClass: 'custom-class custom-loading'
    });
    return await this.loading.present();
  }
  async closeLoading() {
    setTimeout(async () => {
      return await this.loading.dismiss();
    }, 500);

  }
}
