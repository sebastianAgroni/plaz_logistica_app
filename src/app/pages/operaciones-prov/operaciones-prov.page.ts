import { DataService } from './../../servicios/data.service';
import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-operaciones-prov',
  templateUrl: './operaciones-prov.page.html',
  styleUrls: ['./operaciones-prov.page.scss'],
})
export class OperacionesProvPage implements OnInit {

  proveedores: any;

  constructor(private dataService: DataService,
              private navCtrl: NavController) { }


  ngOnInit() {
    this.dataService.getProveedores()
    .subscribe((data) => {
      this.proveedores = data;
    });
  }

  cargPed(idPrv: number) {
    this.navCtrl.navigateRoot('/proveedor/' + idPrv + '/OPER');
  }
}
