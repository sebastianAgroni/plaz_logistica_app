import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-operaciones',
  templateUrl: './operaciones.page.html',
  styleUrls: ['./operaciones.page.scss'],
})
export class OperacionesPage implements OnInit {

  constructor(private navCtrl: NavController) { }

  ngOnInit() {
  }

  admPag(tipPag: string) {
    switch (tipPag) {
      case 'PROV':
        this.navCtrl.navigateRoot('/operaciones-prov');
        break;
      case 'TRAN':
        this.navCtrl.navigateRoot('/transportadores/OPER');
        break;
    }
  }
}
