import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IonicModule } from '@ionic/angular';

import { IngresarPageRoutingModule } from './ingresar-routing.module';

import { IngresarPage } from './ingresar.page';
import { ComponentesModule } from '../../componentes/componentes.module';

@NgModule({
  imports: [
    CommonModule,
    IonicModule,
    IngresarPageRoutingModule,
    ComponentesModule
  ],
  declarations: [IngresarPage]
})
export class IngresarPageModule {}
