import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '', redirectTo: 'inicio', pathMatch: 'full'
  },
  {
    path: 'inicio',
    loadChildren: () => import('./pages/inicio/inicio.module').then(m => m.InicioPageModule)
  },
  {
    path: 'ingresar',
    loadChildren: () => import('./pages/ingresar/ingresar.module').then(m => m.IngresarPageModule)
  },
  {
    path: 'transportadores/:tipP',
    loadChildren: () => import('./pages/transportadores/transportadores.module').then(m => m.TransportadoresPageModule)
  },
  {
    path: 'infopedtran/:idPed/:tipP',
    loadChildren: () => import('./pages/pedido/infopedtran/infopedtran.module').then(m => m.InfopedtranPageModule)
  },
  {
    path: 'proveedor/:nvPrv/:tipP',
    loadChildren: () => import('./pages/proveedor/proveedor.module').then(m => m.ProveedorPageModule)
  },
  {
    path: 'infopedprov/:idPed/:nvPriv/:tipP',
    loadChildren: () => import('./pages/pedido/infopedprov/infopedprov.module').then(m => m.InfopedprovPageModule)
  },
  {
    path: 'operaciones',
    loadChildren: () => import('./pages/operaciones/operaciones.module').then(m => m.OperacionesPageModule)
  },
  {
    path: 'operaciones-prov',
    loadChildren: () => import('./pages/operaciones-prov/operaciones-prov.module').then(m => m.OperacionesProvPageModule)
  },
  {
    path: '**',
    loadChildren: () => import('./pages/inicio/inicio.module').then(m => m.InicioPageModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
