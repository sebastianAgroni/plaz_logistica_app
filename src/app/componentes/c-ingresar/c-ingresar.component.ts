import { DataService } from './../../servicios/data.service';
import { Component, OnInit } from '@angular/core';
import { AlertService } from '../../servicios/alert.service';
import { NavController } from '@ionic/angular';



@Component({
  selector: 'app-c-ingresar',
  templateUrl: './c-ingresar.component.html',
  styleUrls: ['./c-ingresar.component.scss'],
})
export class CIngresarComponent implements OnInit {

  infoUsuario: any;
  tipUsu;
  usuario = {
    lgUsuario: null,
    lgContrasena: null
  };

  constructor(private alertCtrl: AlertService,
              private dataService: DataService,
              private navCtrl: NavController) { }

  ngOnInit() { }

  onIngresar() {
    this.dataService.getUsuario(this.usuario.lgUsuario, this.usuario.lgContrasena)
      .subscribe(data => {
        this.infoUsuario = data;
        if (data[0] == null) {
          this.alertCtrl.alertaError('Usuario incorrecto.');
        } else {
          this.redirigeUsuario(data[0].tipo_persona, data[0].privilegios_usu);
        }
      });
  }

  redirigeUsuario(tipUsu: string, priv: string) {
    switch (tipUsu) {
      case 'TRAN':
        this.navCtrl.navigateRoot('/transportadores/TRAN');
        break;
      case 'PROV':
        this.navCtrl.navigateRoot('/proveedor/' + priv + '/PROV');
        break;
      case 'OPER':
        this.navCtrl.navigateRoot('/operaciones');
        break;
    }
  }
}
