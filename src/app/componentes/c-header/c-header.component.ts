import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-c-header',
  templateUrl: './c-header.component.html',
  styleUrls: ['./c-header.component.scss'],
})
export class CHeaderComponent implements OnInit {

  @Input() titulo: string;
  @Input() subtitulo: string;

  constructor() { }

  ngOnInit() { }

}
